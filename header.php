<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Woocommerce</title>
    <link rel="stylesheet" href="<?php bloginfo('template_url')?>/assets/css/style.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">
    <?php wp_head();?>    
</head>

<body>
    <?php
$cart_count = WC()->cart->get_cart_contents_count();?>
<header class="header">
  <div class="header-top">
    <a href="<?php echo bloginfo('url'); ?>/home" class="a-logo">
      <img src="<?php bloginfo('template_url')?>/assets/img/logo.png" class="logo-company"/>
    </a>
  </div>
  
  <div class="container">
    <a href="<?php echo bloginfo('url'); ?>/home" class="a-logo --main">
      <img src="<?php bloginfo('template_url')?>/assets/img/logo.png" class="logo-company"/>
    </a>
    <div class="search">
      <form action="<?php bloginfo('url'); ?>/loja/" method="get">
        <input type="text" name="s" id="s" placeholder="Buscar" value="<?php the_search_query(); ?>">
        <input type="text" name="post_type" value="product" class="hidden">
        <button type="submit" id="searchbutton" value="Buscar">
        <img src="<?php bloginfo('template_url')?>/assets/img/icon-search.svg" alt="">
        </button>
      </form>
  </div>
  <nav class="nav-account">
    <button id="btn-mobile">
      <span id="hamburguer"></span>
    </button>
    <ul class="menu-list">
      <li class="menu-link">
        <a href="<?php echo bloginfo('url'); ?>/minha-conta" class="minha-conta">
          <img src="<?php bloginfo('template_url')?>/assets/img/icon-myaccount.svg" alt="">
          <p>Minha Conta</p>
        </a>
      </li>
      <li class="menu-link">
        <a href="<?php echo bloginfo('url'); ?>/carrinho" class="carrinho">
          <img src="<?php bloginfo('template_url')?>/assets/img/icon-cart.svg" alt="">
          <p>Carrinho</p>
          <?php if($cart_count) { ?>
            <span class="carrinho-count"><?= $cart_count; ?></span>
          <?php } ?>
        </a>
      </li>
    </ul>    
  </nav>
</header>
<!-- 
<?php
wp_nav_menu([
  'menu' => 'categorias',
  'container' => 'nav',
  'container_class' => 'menu-categorias'
])
?>  -->