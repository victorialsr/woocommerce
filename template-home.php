<?php
/* Template Name: Home */
get_header();
$products_new = wc_get_products([
    'limit' => 4,
    'orderby' => 'date',
    'order' => 'DESC',
  ]);

  $products_sales = wc_get_products([
    'limit' => 9,
    'meta_key' => 'total_sales',
    'orderby'  => 'meta_value_num',
    'order' => 'DESC',
  ]);

$data = [];
$data['novidades'] = format_products($products_new, 'medium');
?>
 
    <section class="section-hero">
        <div class="swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-slide-1">
                    <div class="banner-content">
                        <h1>Lojinha da Vic</h1>
                        <a href="<?php echo bloginfo('url'); ?>/loja" class="btn --swiper">Conheça a nossa loja</a>
                    </div>
                </div>
                <div class="swiper-slide swiper-slide-2">
                    <div class="banner-content">
                    <h1>Lojinha da Vic</h1>
                    <a href="<?php echo bloginfo('url'); ?>/loja" class="btn --swiper">Conheça a nossa loja</a>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-scrollbar"></div>
        </div>
    </section>

    <section class="section-benefits">
        <div class="container">
            <div class="row">
                <div class="bar-benefits">
                    <div class="icon-bar-benefits">
                        <img src="<?php bloginfo('template_url')?>/assets/img/icon-delivery.svg" alt="">
                    </div>
                    <div class="content-bar-benefits">
                        <p>Sem Juros</p>
                        <p>Parcele em até 4x sem juros no cartão de crédito</p>
                    </div>
                </div>
                <div class="bar-benefits">
                    <div class="icon-bar-benefits">
                        <img src="<?php bloginfo('template_url')?>/assets/img/icon-delivery.svg" alt="">
                    </div>
                    <div class="content-bar-benefits">
                        <p>Frete Grátis</p>
                        <p>Para todo o Brasil nas compras acima de R$200,00</p>
                    </div>
                </div>
                <div class="bar-benefits">
                    <div class="icon-bar-benefits">
                        <img src="<?php bloginfo('template_url')?>/assets/img/icon-delivery.svg" alt="">
                    </div>
                    <div class="content-bar-benefits">
                        <p>Lorem ipsum dolor sit</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-name">
        <div class="container">
            <div class="row">
                <div class="card card-1">
                    <div class="card-content">
                        <h3>Kits para presentear</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        <a href="<?php echo bloginfo('url'); ?>/categoria-produto/kits" class="btn">Ver agora</a>
                    </div>
                </div>
                <div class="card card-2">
                    <div class="card-content">
                        <h3>Planners para 2022</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        <a href="<?php echo bloginfo('url'); ?>/categoria-produto/planner-2022" class="btn">Ver agora</a>
                    </div>
                </div>
                <div class="card card-3">
                    <div class="card-content">
                        <h3>A escrita perfeita</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        <a href="<?php echo bloginfo('url'); ?>/categoria-produto/escrita" class="btn">Ver agora</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="section-products">
        <div class="container">
            <div class="products-content --home">
                <h2 class="title-section">Lorem ipsum dolor sit amet</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <?php if(have_posts()) { while (have_posts()) { the_post(); ?>
                <?php company_product_list($data['novidades']); ?>
                <?php }} ?>
                <a href="<?php echo bloginfo('url'); ?>/loja" class="btn --products">Ver todos os produtos</a>
            </div>
        </div>
    </section>
    <section class="section-cta">
        <div class="container">
            <div class="cta-wraper">
                <div class="row">
                    <div class="cta-photo cta-photo-1"></div>
                    <div class="cta-content">
                        <h3>Lorem ipsum dolor sit amet</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <a href="" class="btn --cta">Lorem ipsum dolor sit amet</a>
                    </div>
                </div>
                <div class="row row-2">
                    <div class="cta-content">
                        <h3>Lorem ipsum dolor sit amet</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <a href="" class="btn --cta">Lorem ipsum dolor sit amet</a>
                    </div>
                    <div class="cta-photo cta-photo-2"></div>
                </div>
            </div>

        </div>
    </section>
    <section class="section-insta">
        <div class="row">
            <img src="<?php bloginfo('template_url')?>/assets/img/icon-instagram.svg" alt="">
            <div class="content-insta">
                <p>Siga-nos no Instagram</p>
                <h3>@lojinhadavic</h3>
            </div>

        </div>
    </section>
  
    <section class="section-benefits">
        <div class="container">
            <div class="row">
                <div class="bar-benefits">
                    <div class="icon-bar-benefits">
                        <img src="<?php bloginfo('template_url')?>/assets/img/icon-delivery.svg" alt="">
                    </div>
                    <div class="content-bar-benefits">
                        <p>Sem Juros</p>
                        <p>Parcele até 4x sem juros no cartão de crédito</p>
                    </div>
                </div>
                <div class="bar-benefits">
                    <div class="icon-bar-benefits">
                        <img src="<?php bloginfo('template_url')?>/assets/img/icon-delivery.svg" alt="">
                    </div>
                    <div class="content-bar-benefits">
                        <p>Frete Grátis</p>
                        <p>Para todo o Brasil nas compras acima de R$200,00</p>
                    </div>
                </div>
                <div class="bar-benefits">
                    <div class="icon-bar-benefits">
                        <img src="<?php bloginfo('template_url')?>/assets/img/icon-delivery.svg" alt="">
                    </div>
                    <div class="content-bar-benefits">
                        <p>Lorem ipsum dolor sit</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php get_footer();?>    