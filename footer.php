<footer>
    <div class="row-1 footer">
        <div class="container">
            <nav>
                <ul class="ul-menu">
                    <h3>Menus</h3>
                    <li><a href="<?php echo bloginfo('url'); ?>/home" >Início</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/loja" >Todos os produtos</a></li>
                    <li><a href="">Kits</a></li>
                </ul>
                <ul class="ul-envio">
                    <h3>Métodos de envio</h3>
                    <div>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-envio-correios.png" alt=""></li>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-envio-pac.png" alt=""></li>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-envio-sedex.png" alt=""></li>
                    </div>
                </ul>
                <ul class="ul-pagamento">
                    <h3>Formas de pagamento</h3>
                    <div>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-pag-mastercard.png" alt=""></li>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-pag-visa.png" alt=""></li>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-pag-boleto.png" alt=""></li>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-pag-mercadopago.png" alt=""></li>
                        <li><img src="<?php bloginfo('template_url')?>/assets/img/icon-pag-elo.png" alt=""></li>
                    </div>
                </ul>
                <ul class="ul-contato">
                    <h3>Contato</h3>
                    <li>
                        <a href="tel:551399999999">
                            <img src="<?php bloginfo('template_url')?>/assets/img/icon-whatsapp.svg" alt="">
                            <p>(13) 99999-9999</p>
                        </a>
                    </li>
                    <li>
                        <a href="mailto:contato@lojinhadavic.com.br">
                            <img src="<?php bloginfo('template_url')?>/assets/img/icon-mail.svg" alt="">
                            <p>contato@lojinhadavic.com.br</p>
                        </a>
                    </li>
                </ul>
            </nav>
        </div> 
    </div>
    <div class="copyright-footer">
        <div class="container">
            <p>Lojinha da Vic © 2021 - Todos os direitos reservados </p>
            <div class="kbr-footer">
                <p>Desenvolvido por</p>
                    <a href="https://www.kbrtec.com.br/">
                        <img src="<?php bloginfo('template_url');?>/assets/img/logo-kbrtec.svg" alt="Logo da empresa KBR TEC">
                    </a>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
    <script src="<?php bloginfo('template_url')?>/assets/js/slider.js"></script>
    <script src="<?php bloginfo('template_url')?>/assets/js/script.js"></script>

    <?php wp_footer();?>    
</body>

</html>