<?php
get_header();

$products = [];
if(have_posts()) { while(have_posts()) { the_post();
$products[] = wc_get_product(get_the_ID());
}}
$data = [];
$data['products'] = format_products($products);
?>
<main class="main-archive-product">
<div class="container breadcrumb">
    <?php woocommerce_breadcrumb(['delimiter' => ' > ']);?>
</div>

    <section class="section-products">
        <div class="container">
            <div class="products-intro"> 
                <h2 class="title-section">Lorem ipsum dolor sit amet</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
        </div>
            <div class="products-content">
                <nav class="filter">
                    <h2 class="title-filter">Filtros</h2>
                    <div>
                        <?php
                            wp_nav_menu([
                                'menu' => 'categorias-internas',
                                'menu-class' => 'filter-cat',
                                'container' => 'false',
                            ]);
                        ?>
                    </div>
                    <div>
                    <?php
                        $attribute_taxonomies = wc_get_attribute_taxonomies();

                        foreach($attribute_taxonomies as $attribute) {
                        the_widget('WC_Widget_Layered_Nav', [
                            'title' => $attribute->attribute_label,
                            'attribute' => $attribute->attribute_name,
                        ]);
                        }
                    ?>
                    </div>
                </nav>
               <?php if($data['products'][0]) {?>
                <div class="products-teste">
                <div class="products-teste-form">
                        <?php woocommerce_catalog_ordering(); ?>
                    </div>
                    <?php company_product_list($data['products']);?>
                    <?= get_the_posts_pagination(); ?>
                </div>
                
            <?php } else { ?>
            <p>Nenhum resultado para a sua busca.</p> <?php } ?>
            </div>
        </div>
    </section>
</main>
    <?php
    get_footer();?>