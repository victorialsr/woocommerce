<?php
function company_add_woocommerce_support() {
    add_theme_support('woocommerce');
  }
  add_action('after_setup_theme', 'company_add_woocommerce_support');
function company_custom_images() {
    add_image_size('slide', 1000, 800, ['center', 'top']);
    update_option('medium_crop', 1);
  }
  add_action('after_setup_theme', 'company_custom_images');

  function format_products($products, $img_size = 'mediun') {
    $products_final = [];
    foreach($products as $product) {
      $products_final[] = [
        'name' => $product->get_name(),
        'price' => $product->get_price_html(),
        'link' => $product->get_permalink(),
        'img' => wp_get_attachment_image_src($product->get_image_id(), $img_size)[0],
      ];
    }
    return $products_final;
  }

  function company_product_list($products) { ?>
    <div class="row">
      <?php foreach($products as $product) { ?>
        <div class="card-product">
          <a href="<?= $product['link']; ?>">
          <div class="img-product"><img src="<?= $product['img']; ?>" alt="<?= $product['name']; ?>">
            <h3><?= $product['name']; ?></h3></div>
            <div class="card-content-product">
            <p><?= $product['price']; ?></p> 
          </a>
          <a href="<?= $product['link']; ?>" class="btn --seeMore">Ver mais</a>
        </div>
      </div>
      <?php } ?>
    </div>
  <?php 
  }
  ?>