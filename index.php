<?php get_header(); ?>

<section class="section-index">
    <div class="container">
        <?php if(have_posts()) { while(have_posts()) { the_post(); ?>
            <h1 class="h1-index"><?php the_title(); ?></h1>
            <main><?php the_content(); ?></main>
        <?php }} ?>
    </div>
</section>

<?php get_footer(); ?>
